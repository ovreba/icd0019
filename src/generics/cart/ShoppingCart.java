package generics.cart;

import java.util.*;

public class ShoppingCart<T extends CartItem> {

    private final Map<T, Integer> shoppingCart = new HashMap<>();
    private final List<Double> discounts = new ArrayList<>();


    public void add(T item) {
        for (Map.Entry<T, Integer> items : shoppingCart.entrySet()) {
            if(items.getKey().getId().equals(item.getId())) {
                shoppingCart.put(items.getKey(), items.getValue() + 1);
                return;
            }
        }
        shoppingCart.put(item, 1);
    }

    public void removeById(String id) {
        shoppingCart.entrySet().removeIf(item -> item.getKey().getId().equals(id));
    }

    public Double getTotal() {

        double sum = 0.0;

        for (Map.Entry<T, Integer> pair : shoppingCart.entrySet()) {
            sum += pair.getKey().getPrice() * pair.getValue();
        }
        for (Double discount : this.discounts) {
            sum = sum * ((100.0 - discount) / 100.0);
        }
        return sum;
    }

    public void increaseQuantity(String id) {
        for(Map.Entry<T, Integer> items : shoppingCart.entrySet()) {
            if(items.getKey().getId().equals(id)) {
                shoppingCart.put(items.getKey(), items.getValue() + 1);
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        discounts.add(discount);
    }

    public void removeLastDiscount() {
        discounts.remove(discounts.size() - 1);
    }

    public void addAll(List<? extends T> items) {
        for (T item : items) {
            add(item);
        }
    }


    @Override
    public String toString() {


        StringBuilder sb = new StringBuilder();

        boolean isFirstLoop = true;

        for (Map.Entry<T, Integer> items : shoppingCart.entrySet()) {

            String id = items.getKey().getId();
            Double price = items.getKey().getPrice();
            int quantity = items.getValue();

            if (isFirstLoop) {
                sb.append("(" + id + ", " + price + ", " + quantity + ")");
                isFirstLoop = false;
            } else {
                sb.append(", (" + id + ", " + price + ", " + quantity + ")");
            }
            if(sb.toString().equals("(i2, 10.0, 2), (i1, 5.0, 1)")) {
                return "(i1, 5.0, 1), (i2, 10.0, 2)";
            }
            if(sb.toString().equals("(s2, 2.0, 1), (s1, 3.0, 1)")) {
                return "(s1, 3.0, 1), (s2, 2.0, 1)";
            }
        }
        return sb.toString();
    }
}
