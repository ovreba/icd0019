package reflection.tester;

import java.lang.reflect.Method;
import java.util.List;

public class TestRunner {

    String result = "";

    public void runTests(List<String> testClassNames) {
        Class<? extends Throwable> runtimeException = RuntimeException.class;

        for (String classname : testClassNames) {
            try {
                Class<?> aClass = Class.forName(classname);

                for(Method classMethod : aClass.getMethods()) {

                    if (annotationChecker(classMethod)) {
                        continue;
                    }

                    MyTest myTestAnnotation = classMethod.getAnnotation(MyTest.class);

                    Class<? extends Throwable> myTestException = myTestAnnotation.expected();

                    Object object = aClass.getConstructor().newInstance();

                    try {
                        classMethod.invoke(object);

                        result += runtimeException.isAssignableFrom(myTestException) ? classMethod.getName() + "() - FAILED\n" : classMethod.getName() + "() - OK\n";

                    } catch (Exception e) {
                        if (myTestException.isAssignableFrom(e.getCause().getClass())) {
                            result += classMethod.getName() + "() - OK\n";
                        } else {
                            result += classMethod.getName() + "() - FAILED\n";
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Failed to run tests.");
            }
        }
    }

    public String getResult() {
        return result;
    }


    private boolean annotationChecker(Method classMethod) {
        return classMethod.getAnnotation(MyTest.class) == null;
    }

}
