package inheritance.analyser;

import java.util.List;

public final class FlatTaxSalesAnalyser extends AbstractSalesAnalyser {

    private static final Double FLATTAXRATE = 1.2;

    FlatTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected Double getTaxRate() {
        return FLATTAXRATE;
    }
}
