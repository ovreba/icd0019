package fp.sales;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;

public class Analyser {

    private Repository repository;

    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {
        return repository.getEntries().stream()
                .mapToDouble(Entry::getAmount)
                .sum();
    }

    public Double getSalesByCategory(String category) {
        return repository.getEntries().stream()
                .filter(x -> x.getCategory().equals(category))
                .mapToDouble(Entry::getAmount)
                .sum();
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {
        return repository.getEntries().stream()
                .filter(x -> x.getDate().isBefore(end))
                .filter(x -> x.getDate().isAfter(start))
                .mapToDouble(Entry::getAmount)
                .sum();
    }

    public String mostExpensiveItems() {
        return repository.getEntries().stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(Entry::getAmount)))
                .limit(3)
                .map(Entry::getProductId)
                .sorted()
                .collect(Collectors.joining(", "));
    }

    public String statesWithBiggestSales() {
        return repository.getEntries().stream()
                .collect(Collectors.toMap(
                        Entry::getState,
                        Entry::getAmount,
                        Double::sum)
                ).entrySet()
                .stream()
                .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
                .limit(3)
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(", "));
    }
}
