package oo.hide;


public class PointSet {

    private Point[] points;
    private int index = 0;

    public PointSet(int capacity) {
        points = new Point[capacity];
    }

    public PointSet() {
        this(10);
    }

    public void add(Point point) {
        if (size() == points.length) {
            points = new Point[size() * 2];
        }
        if (!(contains(point))) {
            points[index] = point;
            index++;
        }
    }

    public int size() {
        int size = 0;

        for (Point p : points) {
            if (p != null) {
                size++;
            }
        }
        return size;
    }

    public boolean contains(Point point) {
        for (Point p : points) {
            if (p != null && p.equals(point)) {
                return true;
            }
        }
        return false;
    }

    public PointSet subtract(PointSet other) {
        PointSet finalArray = new PointSet();

        for (Point p : points) {
            if (!(other.contains(p))) {
                finalArray.add(p);
            }
        }
        return finalArray;
    }

    public PointSet intersect(PointSet other) {
        PointSet finalArray = new PointSet();

        for (Point p : points) {
            if (other.contains(p)) {
                finalArray.add(p);
            }
        }
        return finalArray;
    }

    @Override
    public String toString() {
        String finalString = "";
        int notLastLoop = size();

        for (Point p : points) {
            if (p != null) {
                finalString += p.toString();
                notLastLoop--;
                if (notLastLoop != 0) {
                    finalString += ", ";
                }
            }
        }
        return finalString;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PointSet)) {
            return false;
        }

        PointSet other = (PointSet) obj;

        for (Point p : points) {
            if (p != null && !(other.contains(p))) {
                return false;
            }
        }
        return true;
    }
}

