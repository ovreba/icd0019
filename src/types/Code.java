package types;

import java.util.Arrays;
import java.util.Random;

public class Code {

    public static void main(String[] args) {

        int[] numbers = {1, 3, -2, 9};

        // System.out.println(asString(numbers)); // 11
        System.out.println(mode("abccbc")); // c
    }

    public static int sum(int[] numbers) {
        Integer sum = 0;
        for (int number : numbers) {
            sum += number;

        }
        return sum;
    }

    public static double average(int[] numbers) {
        Double sum = 0.0;
        for (int number : numbers) {
            sum += number;

        }
        return sum / numbers.length;
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0) {
            return null;
        }

        Integer min = integers[0];

        for (int each : integers) {

            if (each < min) {
                min = each;
            }
        }
        
        return min;
    }

    public static String asString(int[] elements) {
        String result = "";

        for (int element : elements) {

            if (element == elements[elements.length - 1])  {
                result += element;
            } else {
            result += element + ", ";
            }
        }

        return result;
    }

    public static Character mode(String input) {

        if (input.equals("")) {
            return null;
        }

        int[] count = new int[256];
        int len = input.length();

        for (int i=0; i<len; i++) {
            count[input.charAt(i)]++;
        }

        int max = 0;
        char result = ' ';

        for (int i = 0; i < len; i++) {
            if (max < count[input.charAt(i)]) {
                max = count[input.charAt(i)];
                result = input.charAt(i);
            }
        }

        return result;
    }

    public static String squareDigits(String s) {
        String result = "";

        for (int i = 0; i < s.length(); i++){
            if (Character.isDigit(s.charAt(i))) {
                String charToString = Character.toString(s.charAt(i));
                int toSquare = Integer.parseInt(charToString);
                result += toSquare * toSquare;
            } else {
                result += Character.toString(s.charAt(i));
            }

        }
        return result;
    }

    public static int isolatedSquareCount() {
        boolean[][] matrix = getSampleMatrix();

        printMatrix(matrix);

        int isolatedCount = 0;

        // count isolates squares here

        return isolatedCount;
    }

    private static void printMatrix(boolean[][] matrix) {
        for (boolean[] row : matrix) {
            System.out.println(Arrays.toString(row));
        }
    }

    private static boolean[][] getSampleMatrix() {
        boolean[][] matrix = new boolean[10][10];

        Random r = new Random(5);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = r.nextInt(5) < 2;
            }
        }

        return matrix;
    }
}
