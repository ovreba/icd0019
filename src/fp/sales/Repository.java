package fp.sales;


import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Repository {

    private static final String FILE_PATH = "src/fp/sales/sales-data.csv";

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("dd.MM.yyyy");


    public List<Entry> getEntries() {

        List<Entry> entries;

        try {

            File inputF = new File(FILE_PATH);

            InputStream inputFS = new FileInputStream(inputF);

            BufferedReader br = new BufferedReader(new InputStreamReader(inputFS));

            entries = br.lines().skip(1).map(mapToEntry).collect(Collectors.toList());

            br.close();

        } catch (IOException e) {
            throw new RuntimeException("Failed to read file." + e);
        }
        return entries;
    }


    private Function<String, Entry> mapToEntry = (line) -> {

        String[] data = line.split("\t");

        Entry entryToAdd = new Entry();

        entryToAdd.setDate(LocalDate.parse(data[0], formatter));

        entryToAdd.setState(data[1]);

        entryToAdd.setProductId(data[2]);

        entryToAdd.setCategory(data[3]);

        entryToAdd.setAmount(Double.parseDouble(data[5].replaceAll(",", ".")));

        return entryToAdd;

    };

}

