package inheritance.analyser;

import java.util.List;

public abstract sealed class AbstractSalesAnalyser
                        permits DifferentiatedTaxSalesAnalyser, FlatTaxSalesAnalyser, TaxFreeSalesAnalyser {

    protected final List<SalesRecord> records;

    protected AbstractSalesAnalyser(List<SalesRecord> records){
        this.records = records;
    }

    protected abstract Double getTaxRate();


    protected Double getTotalSales() {
        double sum = 0.0;

        for (SalesRecord record : records) {
            sum += record.getProductPrice() / getTaxRate() * record.getItemsSold();
        }
        return sum;
    }

    protected Double getTotalSalesByProductId(String id) {
        double sum = 0.0;

        for (SalesRecord record : records) {
            if (record.getProductId().equals(id)) {
                sum += record.getProductPrice() * record.getItemsSold();
            }
        }
        return sum / getTaxRate();
    }
    protected String getIdOfMostPopularItem() {
        int currentBest = 0;
        String resultId = "";

        for (SalesRecord record : records) {
            if (record.getItemsSold() > currentBest) {
                currentBest = record.getItemsSold();
                resultId = record.getProductId();
            }
        }
        return resultId;
    }

    protected String getIdOfItemWithLargestTotalSales() {
        int currentBest = 0;
        String resultId = "";

        for (SalesRecord record : records) {
            if (record.getItemsSold() * record.getProductPrice() > currentBest) {
                currentBest = record.getItemsSold() * record.getProductPrice();
                resultId = record.getProductId();
            }
        }
        return resultId;
    }

}
