package exceptions.numbers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;


public class NumberConverter {

    private Properties properties;


    public NumberConverter(String lang) {
        properties = readFile(lang);
        if(!checkFileValidity()) {
            throw new MissingTranslationException(lang);
        }
    }

    public String numberInWords(Integer number) {
        String result = "";
        if (number >= 100) {
            result += properties.getProperty(String.valueOf(number / 100)) +
                    properties.getProperty("hundreds-before-delimiter") + properties.getProperty("hundred");
            number -= (number / 100) * 100;
            if (!number.equals(0)) {
                result += properties.getProperty("hundreds-after-delimiter");
            }
        }
        if (number < 100 && number >= 20) {
            return translateOverTwenty(number, result);
        }
        if (number < 20 && number > 10) {
            if (properties.containsKey(String.valueOf(number))) {
                result += properties.getProperty(String.valueOf(number));
            } else {
                result += properties.getProperty(String.valueOf(number % 10)) + properties.getProperty("teen");
            }
        }
        if (number <= 10) {
            if (!"".equals(result) && number == 0) {
                return result;
            }
            result += properties.getProperty(String.valueOf(number));
        }
        return result;
    }

    private String translateOverTwenty(Integer number, String currentResult) {

        String tens = number / 10 + "0";

        String ones = String.valueOf(number - Integer.parseInt(tens));

        if (properties.containsKey(String.valueOf(number))) {
            currentResult += properties.getProperty(String.valueOf(number));
        } else if (properties.containsKey(tens)) {
            currentResult += properties.getProperty(tens) + properties.getProperty("tens-after-delimiter") +
                    properties.getProperty(ones);
        } else {
            currentResult += properties.getProperty(String.valueOf(number / 10)) + properties.getProperty("tens-suffix");
            if (!"0".equals(ones)) {
                currentResult += properties.getProperty("tens-after-delimiter") + properties.getProperty(ones);
            }
        }
        return currentResult;
    }

    private Properties readFile(String lang) {
        String filePath = String.format("src/exceptions/numbers/numbers_%s.properties", lang);

        Properties properties = new Properties();

        FileInputStream is;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(is, StandardCharsets.UTF_8);

            try {
                properties.load(reader);
            } catch (IllegalArgumentException e) {
                throw new BrokenLanguageFileException(lang, e);
            }
            is.close();
        } catch(NullPointerException | IOException e) {
            throw new MissingLanguageFileException(lang, e);
        }
        return properties;
    }

    private boolean checkFileValidity() {
        return this.properties.containsKey("0") && this.properties.containsKey("1") && this.properties.containsKey("10")
                && this.properties.containsKey("hundred");
    }
}
