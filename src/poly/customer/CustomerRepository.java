package poly.customer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerRepository {

    private static final String FILE_PATH = "src/poly/customer/data.txt";

    private List<AbstractCustomer> customers = new ArrayList<>();

    private DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("yyyy-MM-dd");

    {
        try {
            List<String> lines = Files.readAllLines(Paths.get(FILE_PATH));

            for (String line : lines) {

                String[] data = line.split(";");

                if (data[0].equals("REGULAR")) {
                    customers.add(new RegularCustomer(data[1], data[2], Integer.parseInt(data[3]), LocalDate.parse(data[4], formatter)));

                } else {
                    customers.add(new GoldCustomer(data[1], data[2], Integer.parseInt(data[3])));
                }
            }

        } catch (IOException e) {
            throw new RuntimeException("Failed to read file " + e);
        }
    }

    public Optional<AbstractCustomer> getCustomerById(String id) {

        for (AbstractCustomer customer : customers) {
            if (customer.getId().equals(id)) {
                return Optional.of(customer);
            }
        }
        return Optional.empty();
    }

    public void remove(String id) {
        customers.removeIf(customer -> customer.getId().equals(id));
    }

    public void save(AbstractCustomer customer) {
        Optional<AbstractCustomer> currentCustomer = getCustomerById(customer.getId());

        if (currentCustomer.isEmpty()) {
            customers.add(customer);
        } else {
            remove(currentCustomer.get().getId());
            customers.add(customer);
        }

        try {
            File inputF = new File(FILE_PATH);

            FileOutputStream fos = new FileOutputStream(inputF);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            for (AbstractCustomer customerToWrite : customers) {
                bw.write(customerToWrite.asString());
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            throw new RuntimeException("Failed to write customers " + e);
        }
    }

    public int getCustomerCount() {
        return customers.size();
    }
}
