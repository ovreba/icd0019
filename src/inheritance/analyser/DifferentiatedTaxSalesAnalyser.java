package inheritance.analyser;

import java.util.List;

public final class DifferentiatedTaxSalesAnalyser extends AbstractSalesAnalyser {

    private static final Double REGULARTAXRATE = 1.2;
    private static final Double REDUCEDTAXRATE = 1.1;

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    protected Double getTaxRate() {
        return null;
    }

    @Override
    protected Double getTotalSales() {
        double sum = 0.0;

        for (SalesRecord record : records) {
            if (record.hasReducedRate()) {
                sum += (record.getProductPrice()) / REDUCEDTAXRATE * record.getItemsSold();
            } else {
                sum += (record.getProductPrice()) / REGULARTAXRATE * record.getItemsSold();
            }
        }
        return sum;
    }

}

