package poly.customer;

import java.time.LocalDate;
import java.util.Objects;

public final class RegularCustomer extends AbstractCustomer {

    private LocalDate lastOrder;

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);

        this.lastOrder = lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {

        double total = order.getTotal();

        double multiplier = 1.0;

        if (total >= 100) {
            if (order.getDate().getMonthValue() - lastOrder.getMonthValue() < 1) {
                multiplier = 1.5;
            }
            super.bonusPoints += total * multiplier;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        AbstractCustomer other = (AbstractCustomer) obj;

        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) &&
                Objects.equals(bonusPoints, other.bonusPoints);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints);
    }

    @Override
    public String asString() {
        return String.format("REGULAR;%s;%s;%d;%s;", id, name, bonusPoints, this.lastOrder);
    }

}