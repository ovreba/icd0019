package collections.simulator;

import java.util.*;
import static collections.simulator.Card.CardValue.*;

public class Hand implements Iterable<Card>, Comparable<Hand> {

    private List<Card> cards = new ArrayList<>();

    public Map<Card.CardValue, Integer> cardValueMap = new HashMap<>();


    public void addCard(Card card) {
        cards.add(card);
        cardValueMap.put(card.getValue(), cardValueMap.getOrDefault(card.getValue(), 0) + 1);
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {
        if (isFourOfKind()) {
            return HandType.FOUR_OF_A_KIND;
        } else if (isFullHouse()) {
            return HandType.FULL_HOUSE;
        } else if (isStraightFlush()) {
            return HandType.STRAIGHT_FLUSH;
        } else if (isFlush()) {
            return HandType.FLUSH;
        } else if (isStraight()) {
            return HandType.STRAIGHT;
        } else if (isTrips()) {
            return HandType.TRIPS;
        } else if (isTwoPairs()) {
            return HandType.TWO_PAIRS;
        } else if (isPair()) {
            return HandType.ONE_PAIR;
        }
        return HandType.HIGH_CARD;
    }

    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    @Override
    public int compareTo(Hand other) {
        return 0;
    }

    public boolean isPair(){
        return Collections.frequency(cardValueMap.values(), 2) == 1;
    }

    public boolean isTwoPairs(){
        return Collections.frequency(cardValueMap.values(), 2) == 2;
    }

    public boolean isTrips(){
        return Collections.frequency(cardValueMap.values(), 3) == 1;
    }

    public boolean isFullHouse() {
        return cardValueMap.containsValue(2) && cardValueMap.containsValue(3);
    }

    public boolean isFourOfKind() {
        return Collections.frequency(cardValueMap.values(), 4) == 1;
    }

    public boolean isStraight() {
        Collections.sort(cards);

        if (!isFullHand()) {
            return false;
        }

       if (cardValueMap.containsKey(S2) && cardValueMap.containsKey(S3) && cardValueMap.containsKey(S4) && cardValueMap.containsKey(S5)
                && cardValueMap.containsKey(A)) {
            return true;
       } else {
           for (int i = 0; i < cards.size() - 1; i++) {
               if (cards.get(i + 1).getValue().ordinal() - cards.get(i).getValue().ordinal() != 1) {
                   return false;
               }
           }
           return true;
       }
    }

    public boolean isFlush(){

        if (!isFullHand()) {
            return false;
        }


        for (int i = 0; i < cards.size() - 1; i++) {
            if (cards.get(i + 1).getSuit() != cards.get(i).getSuit()){
                return false;
            }
        }
        return true;
    }

    public boolean isStraightFlush() {
        return isStraight() && isFlush();
    }

    public boolean isFullHand() {
        return cards.size() == 5;
    }
}
