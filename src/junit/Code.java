package junit;

public class Code {

    public static boolean isSpecial(int candidate) {
        return candidate % 11 < 4;
    }

    public static int longestStreak(String inputString) {
        if (inputString == null) {
            return 0;
        }


        int longest = 0;
        Character lastChar = null;
        int currentStreakLength = 0;

        for (Character currentChar : inputString.toCharArray()) {
            if (currentChar.equals(lastChar)) {
                currentStreakLength++;
            } else {
                currentStreakLength = 1;
            }

            if (currentStreakLength > longest) {
                longest = currentStreakLength;
            }

            lastChar = currentChar;
        }


        return longest;
    }

    public static Character mode(String inputString) {
        if (inputString == null) {
            return null;
        }

        int modeCount = 0;
        Character mode = null;
        for (char c1 : inputString.toCharArray()) {

            int count = getCharacterCount(inputString, c1);

            if (count > modeCount) {
                modeCount = count;
                mode = c1;
            }
        }
        return mode;
    }

    public static int getCharacterCount(String allCharacters, char targetCharacter) {
        int count = 0;
        for (char each : allCharacters.toCharArray()) {
            if (each == targetCharacter) {
                count++;
            }
        }
        return count;
    }

    public static int[] removeDuplicates(int[] integers) {
        int[] nonDuplicates = new int[integers.length];
        int counter = 0;
        boolean hasBeenZero = false;

        for (int i : integers) {
            if (!(checkIfNumberInArray(nonDuplicates, i))) {
                nonDuplicates[counter] = i;
                counter++;
            } else if(i == 0 && !hasBeenZero) {
                nonDuplicates[counter] = i;
                counter++;
                hasBeenZero = true;
            }
        }
        return copyArray(nonDuplicates, counter);
    }
    
    private static int[] copyArray(int[] integers, int newlength) {
        // Makes copy of array, input is (old array, new array length)
        int[] copiedArray = new int[newlength];
        for (int i = 0; i < newlength; i++) {
            copiedArray[i] = integers[i];
        }
        
        return copiedArray;
    }

    private static boolean checkIfNumberInArray(int[] integers, int number) {
        for (int i : integers) {
            if (i == number) {
                return true;
            }
        }
        return false;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        int[] nonDuplicates = removeDuplicates(integers);
        int sum = 0;
        for (int i : nonDuplicates) {
            sum += i;
        }
        return sum;
    }

}
