package oo.hide;

public class Timer {

    private long start = System.currentTimeMillis();

    public String getPassedTime() {
        double diff = System.currentTimeMillis() - start;

        return String.format("%s sek", diff / 1000);
    }
}
