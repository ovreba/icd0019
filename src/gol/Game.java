package gol;

import java.util.ArrayList;
import java.util.List;

public class Game {

    List<Point> board = new ArrayList<>();

    public void markAlive(int x, int y) {
        board.add(new Point(x, y));
    }

    public boolean isAlive(int x, int y) {
        for (Point cell : board) {
            if (cell.x == x && cell.y == y) {
                return true;
            }
        }
        return false;
    }

    public void toggle(int x, int y) {
        if (isAlive(x ,y)) {
            board.remove(new Point(x, y));
        } else {
            board.add(new Point(x, y));
        }
    }

    public Integer getNeighbourCount(int x, int y) {

        int neighbours = 0;

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if(isAlive(x - i, y - j) && !(i == 0 && j == 0)) {
                    neighbours += 1;
                }
            }
        }
        return neighbours;
    }

    public void nextFrame() {

        List<Point> tempBoard = new ArrayList<>();

        int fieldSize = board.size();

        for (int i = 0; i < fieldSize; i++) {
            for (int j = 0; j < fieldSize; j++) {
                if(nextState(isAlive(i, j), getNeighbourCount(i, j))) {
                    tempBoard.add(new Point(i, j));
                }
            }
        }

        board = tempBoard;
    }

    public void clear() {
        board = new ArrayList<>();
    }

    public boolean nextState(boolean isLiving, int neighborCount) {
        if(!isLiving && neighborCount == 3) {
            return true;
        }
        if(isLiving) {
            return neighborCount >= 2 && neighborCount <= 3;
        }
        return false;
    }
}
